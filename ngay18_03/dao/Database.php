<?php
class Database
{
    public $productTable = array();
    public $categoryTable = array();
    public $accessotionTable = array();
    private $instants;

    public function insertTable( $name,  $row)
    {
        $this->{$name}[] = $row;
    }

    public function selectTable( $name,  $whereId = null)
    {
        if(!empty($where))
        {
            foreach($this->{$name} as $value)
            {
                if($value->getId() == $whereId->getId())
                {
                    return $value;
                }
            }
        }
        return $this->{$name};
    }

    public function updateTable( $name,  $row)
    {
        foreach ($this->{$name} as $key=>$value)
        {
            if($this->{$name}[$key]->getId() == $row->getId())
            {
                $this->{$name}[$key] = $row;
            }
        }
    }

    public function deleteTable( $name,  $row)
    {
        foreach ($this->{$name} as $key=>$value)
        {
            if($this->{$name}[$key]->getId() == $row->getId())
            {
                unset($this->{$name}[$key]);
            }
        }
    }

    public function truncateTable( $name)
    {
        $this->{$name} = null;
    }

    public function updateTableById( $id,  $row)
    {
        switch(get_class($row))
        {
            case 'Product' :
                foreach($this->productTable as $key=>$product)
                {
                    if($product->getId() == $id)
                    {
                        $this->productTable[$key] = $row;
                    }
                }
                break;
            case 'Category' :
                foreach($this->categoryTable as $key=>$category)
                {
                    if($category->getId() == $id)
                    {
                        $this->categoryTable[$key] = $row;
                    }
                }
                break;
            case 'Accessotion' :
                foreach($this->accessotionTable as $key=>$accessotion)
                {
                    if($accessotion->getId() == $id)
                    {
                        $this->accessotionTable[$key] = $row;
                    }
                }
                break;
        }
    }

}
