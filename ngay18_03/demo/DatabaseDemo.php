<?php
require('./../dao/Database.php');
require('./../entity/Product.php');
require('./../entity/Category.php');
require('./../entity/Accessotion.php');

class DatabaseDemo extends Database
{
    public function insertTableTest( $name,  $row)
    {
        $this->insertTable($name, $row);
    }
    public function selectTableTest( $name,  $whereId=null)
    {
        return $this->selectTable($name, $whereId);
    }
    public function updateTableTest( $name,  $row)
    {
        $this->updateTable($name, $row);
    }
    public function deleteTableTest( $name,  $row)
    {
        $this->deleteTable($name, $row);
    }
    public function truncateTableTest( $name)
    {
        $this->truncateTable($name);
    }
    public function updateTableByIdTest( $id,  $row)
    {
        $this->updateTableById($id, $row);
    }
    public function initDatabase()
    {
        for($i = 1; $i<=10 ; $i++)
        {
            $product = new Product($i, 'Bánh ngọt '.$i, 2);
            $this->insertTable('productTable', $product);

            $category = new Category($i, 'Loại bánh '.$i);
            $this->insertTable('categoryTable', $category);

            $accessotion = new Accessotion($i, 'Accessotion '.$i);
            $this->insertTable('accessotionTable', $accessotion);
        }
    }
}
$database = new DatabaseDemo();
$database->initDatabase();
echo '<pre>';
print_r(($database->selectTableTest('categoryTable')));
