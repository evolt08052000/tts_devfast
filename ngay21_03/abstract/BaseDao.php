<?php
require_once('./../interface/IDao.php');
require_once('./../dao/Database.php');
require_once('./../entity/Product.php');
require_once('./../entity/Category.php');
require_once('./../entity/Accessotion.php');

abstract class BaseDao implements IDao
{
    protected $database;

    public function __construct()
    {
        $this->database = Database::getInstants();
    }

    /**
     * Get name table from param row
     * @param $row
     * @return string
     */
    public function getTableName($row)
    {
        return strtolower(get_class($row)).'Table';
    }

    /**
     * Insert row to productTable
     * @param $row
     * @return void
     */
    public function insert($row)
    {
        $this->database->insertTable($this->getTableName($row), $row);
    }

    /**
     * Update row productTable by ID
     * @param $row
     * @return void
     */
    public function update($row)
    {
        $this->database->updateTable($this->getTableName($row), $row);
    }

    /**
     * delete row from productTable by ID
     * @param $row
     * @return void
     */
    public function delete($row)
    {
        $this->database->deleteTable($this->getTableName($row), $row);
    }

    /**
     * Select All row productTable
     * @return mixed
     */
    public function findAll($name)
    {
        return $this->database->selectTable($name);
    }

    /**
     * Select row by ID productTable
     * @return mixed
     */
    public function findById($name, $id)
    {
        return $this->database->selectTable($name, $id);
    }

    public function initDatabase()
    {
        for($i = 1; $i<=10 ; $i++)
        {
            $product = new Product($i, 'Bánh ngọt '.$i, 2);
            $this->insert($product);

            $category = new Category($i, 'Loại bánh '.$i);
            $this->insert($category);

            $accessotion = new Accessotion($i, 'Accessotion '.$i);
            $this->insert($accessotion);
        }
    }

}