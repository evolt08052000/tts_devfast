<?php
require_once('./../entity/Product.php');

class ProductDemo extends Product
{
    public function __construct()
    {
       
    }

    /**
     * Create instants Product
     * @param  $id
     * @param  $name
     * @param  $categoryId
     * return mixed
     */
    public function createProductTest($id, $name, $categoryId)
    {
        $product =  new Product($id, $name, $categoryId);
        return $product;
    }

    /**
     * Create instants Product
     * @param Product $product
     * return mixed
     */
    public function printProduct(Product $product)
    {
        echo $product->getId();
        return $product;
    }
}
